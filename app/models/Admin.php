<?php

/*
 *  brainchild > Admin.php
 *  =========================
 *
 *  :copyright: (c) 2016-17 by BASH Labs Private Limited.
 *  :license: BASH Labs Private License. See LICENSE.md for more details.
 */

class Admin
{
    var $db = NULL;

    public function __construct(&$db)
    {
        $this->db = &$db;
    }

    public function getDeptCoDetails()
    {
        $result = $this->db->select('departments',"*");
        return $result;
    }

    public static function getStat($db){

        $count = $db->count("users",[
           "type"=>USER_TYPE::$STUDENT_INTERNAL
        ]);
        $stat["ku_reg"]=$count;

        $count = $db->count("users",[
            "type"=>USER_TYPE::$STUDENT_EXTERNAL
        ]);

        $stat["ex_reg"]=$count;
        return $stat;
    }

    public static function getRegistration($db){
        $data = $db->select("users",[
            "id","user","name","phone","college"
        ],[
            "type"=>USER_TYPE::$STUDENT_INTERNAL
        ]);

        return $data;
    }

}