<?php

/*
 *  brainchild > Enums.php
 *  =========================
 *
 *  :copyright: (c) 2016-17 by BASH Labs Private Limited.
 *  :license: BASH Labs Private License. See LICENSE.md for more details.
 */

class USER_TYPE {

    // Usertypes identification class

    static $ADMIN = 99;
    static $STUDENT_EXTERNAL = 1;
    static $STUDENT_INTERNAL = 2;
    static $MEGA = 3;
    static $DIG = 4;
    static $DEPARTMENT = 5;
    static $EVENT_CO = 6;
    static $GAMES_CO = 7;
    static $PRIZE_CO = 8;
    static $ID_CARD = 9;
    static $REGISTRATION = 10;
    static $ACCOMODATION = 11;

    public static function getType($id) {
      
        // Return relevant type name
        // :param: $id - ID of user type

        if ($id === self::$ADMIN) {
            return "MK Administrator";
        } elseif ($id === self::$EVENT_CO) {
            return "Event Coordinator";
        } elseif ($id === self::$ACCOMODATION) {
            return "Accommodations";
        } elseif ($id === self::$DEPARTMENT) {
            return "Department Coordinator";
        } elseif ($id === self::$ID_CARD) {
            return "ID Card Issuer";
        } elseif ($id === self::$GAMES_CO) {
            return "Game Coordinator";
        } elseif ($id === self::$MEGA) {
            return "Core Coordinator";
        } elseif ($id === self::$REGISTRATION) {
            return "Registration Desk";
        } elseif ($id === self::$PRIZE_CO) {
            return "Prize Distributor";
        } elseif ($id === self::$DIG) {
            return "Dignitaries";
        } else {
            return "Unpossible";
        }
    }

}


class EVENT_TYPE {

    // Event type identification class

    static $TECHNICAL = 0;
    static $NONTECHNICAL = 1;
    static $WORKSHOP = 2;
    static $GAMES = 3;

    public static function getName($id) {

        if ($id === self::$GAMES){
            return "Game";
        }else if ($id === self::$NONTECHNICAL){
            return "Non Technical";
        }else if ($id === self::$TECHNICAL){
            return "Technical";
        }else if ($id === self::$WORKSHOP){
            return "Workshop";
        }
    }

}


class EVENT_STATUS {

    // Event status identification class
    static $APPROVAL_PENDING = 0;
    static $OPEN = 1;
    static $CLOSED = 2;
    static $STARTED = 3;
    static $COMPLETED = 4;
    static $PRIZE = 5;
    static $OVER = 6;

    public static function getName($id) {

        // Returns relevant text information
        // :param: $id - Id of the event status

        if ($id === self::$OPEN) {
            return "Registration Open";
        } elseif ($id === self::$CLOSED) {
            return "Registration Closed";
        } elseif ($id === self::$STARTED) {
            return "Event Started";
        } elseif ($id === self::$COMPLETED) {
            return "Contest Completed";
        } elseif ($id === self::$PRIZE) {
            return "Winners Selected";
        } elseif ($id === self::$OVER) {
            return "Event Completed";
        } elseif ($id === self::$APPROVAL_PENDING) {
            return "Approval Pending";
        }
    }

    public static function getColor($id) {

        // Returns relevant text information
        // :param: $id - Id of the event status

        if ($id === self::$OPEN) {
            return "success";
        } elseif ($id === self::$CLOSED) {
            return "danger";
        } elseif ($id === self::$STARTED) {
            return "info";
        } elseif ($id === self::$COMPLETED) {
            return "primary";
        } elseif ($id === self::$PRIZE) {
            return "warning";
        } elseif ($id === self::$OVER) {
            return "purple";
        } elseif ($id === self::$APPROVAL_PENDING) {
            return "info";
        }
    }

}


class NOTIFICATION_STATUS {

    // Notification status identification class

    static $UNREAD = 0;
    static $READ = 1;

}


class USER_STATUS {

    // Pay status identification class

    static $CREATED = 0;
    static $OTP_SENT = 1;
    static $VERIFIED = 2;
    static $PAYED = 3;
    static $IDNOTISSUED = 2;
    static $IDISSUED = 3;

}


class USER_STATUS_CHECK {

    // User status check integer

    static $STUDENT = 3;
    static $OTP_VERIFIED = 2;

}


class MINDKRAFT {

    // MK main status identification class

    static $UNDER_CONSTRUCTION = 0;
    static $ITS_A_GO = 1;

}


class COLLEGE {

    // Home college

    static $HOME = "Karunya University";

}


class OTP_STATUS {

    // OTP Status

    static $PENDING = 0;
    static $VERIFIED = 1;

}


class SMS_TEMPLATES {

    // SMS templates

    public static function OTP($name, $otp) {
        return "$name your verification code is : $otp. Visit http://mindkraft.org to Login.";
    }

    public static function EVENT_INFO($event, $venue, $time) {
        return "$event has started at $venue $time. Hurry up and all the best!\nRegards: MK17 Team.";
    }

    public static function EVENT_PROMO($event, $venue ,$msg) {
        return "$event at $venue. $msg";
    }

    public static function EVENT_RESULT($name, $event, $venue, $time, $place) {
        return "Congrats $name you have won $place in $event. Prizes are distrubuted at $venue @ $time.";
    }

    public static function EVENT_ATTENDENCE($name, $event, $venue) {
        return "$name, you have attended $event @ $venue.";
    }

    public static function RESET_PASSWORD($code) {
        return "Your password reset code is : $code.";
    }

}
