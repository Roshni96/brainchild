<?php

/*
 *  brainchild > Event.php
 *  =========================
 *
 *  :copyright: (c) 2016-17 by BASH Labs Private Limited.
 *  :license: BASH Labs Private License. See LICENSE.md for more details.
 */

include_once "Enums.php";

class Event
{
    var $db = NULL;

    public function __construct(&$db)
    {
        $this->db = &$db;
    }

    public function getEvents($dept)
    {
        $result = $this->db->select('events','*',[
            "dept" => $dept
        ]);
        return $result;
    }

    public function getEventsbyType($dept, $type)
    {
        $result = $this->db->select('events','*',[
            'AND' => [
                "dept" => $dept,
                "type" => $type
            ]
        ]);
        return $result;
    }


    public static function approveEvent($db, $id)
    {
        $result = $db->update("events",[
            "status"=>EVENT_STATUS::$OPEN
        ],[
            "id"=>$id
        ]);

        return ['status'=>"success","message"=>"Approved"];
    }

    public static function allowEditsEvent($db, $id)
    {
        $result = $db->update("events",[
            "status"=>EVENT_STATUS::$APPROVAL_PENDING
        ],[
            "id"=>$id
        ]);

        return ['status'=>"success","message"=>"Approved"];
    }

    public static function getEventsbyDeptVerfied($db, $dept)
    {
        $Parsedown = new Parsedown();
        $dept_list = [];
        $result = $db->select('events','*',[
            'AND' => [
                "dept" => $dept,
                "status[>]" => EVENT_STATUS::$APPROVAL_PENDING
            ]
        ]);
        foreach ($result as $r)
        {
            $user = $db->select("users",'*',[
                "id"=>intval($r['user_id'])
            ]);
            $r['co_name'] = $user[0]['name'];
            $r['description'] = $Parsedown->text($r['description']);
            array_push($dept_list, $r);
        }
        return $dept_list;
    }

    public static function getEventsbyDept($db, $dept)
    {
        $dept_list = [];
        $result = $db->select('events','*',[
            'AND' => [
                "dept" => $dept
            ]
        ]);
        foreach ($result as $r)
        {
            $user = $db->select("users",'*',[
                "id"=>intval($r['user_id'])
            ]);
            $r['co_name'] = $user[0]['name'];
            array_push($dept_list, $r);
        }
        return $dept_list;
    }

    public function getGames()
    {
        $result = $this->db->select('events','*',[
            "type" => EVENT_TYPE::$GAMES
        ]);
        return $result;
    }

    public function getWorkshops()
    {
        $result = $this->db->select('events','*',[
            "type" => EVENT_TYPE::$WORKSHOP
        ]);
        return $result;
    }

    public static function getEventRegistrationCount($db, $id)
    {
        $count = $db->count("event_registration",[
            "event_id"=>$id
        ]);

        return $count;
    }

    public static function registrations($db, $id)
    {
        $data = $db->select("users",[
            "id","name","phone","college","username"
        ],[
           "id" => $db->select("event_registration","user_id",[
             "event_id"=>$id
           ])
        ]);
        return $data;
    }

    public static function getEventDetails($db, $user_id)
    {
        $dept = $db->select('events','*',[
            "user_id" => $user_id
        ]);

        if(count($dept)){
            return $dept[0];
        }

        return 0;
    }

}